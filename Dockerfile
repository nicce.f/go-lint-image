# Base image:
FROM golang:1.11-alpine
LABEL maintainer="Niclas Fredriksson <nicce.f@gmail.com>"

#install git
RUN apk --update add git

# Install golint
RUN go get -u golang.org/x/lint/golint

# Install gosec
RUN go get github.com/securego/gosec/cmd/gosec

# Enable go module
ENV GO111MODULE on
ENV CGO_ENABLED="0"


